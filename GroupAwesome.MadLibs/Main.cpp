#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
#include <ctype.h>

using namespace std;

void SaveText(string text)
{
	// A string passed into this function will be saved as plaintext to a relative path into a file named "test.txt"
	string filepath = "test.txt";
	ofstream ofs(filepath);
	ofs << text;
	ofs.close();
	cout << "Your file has been saved as: " + filepath + "\n";
	cout << "Press any key to exit...";
	(void)_getch();
}

string* BuildMadLib(string* userWords) {
	string *input = new string;
	*input += "A vacation is when you take a trip to some " +
		userWords[0] + " place with your " + 
		userWords[1] + " family.\nUsually you go to some place that is near a/an " + 
		userWords[2] + " or up on a/an " +
		userWords[3] + ".\nA good vacation place is one where you can ride " +
		userWords[4] + " or play " +
		userWords[5] + " or go hunting for " +
		userWords[6] + ".\nI like to spend my time " +
		userWords[7] + " or " +
		userWords[8] + ".\nWhen parents go on a vacation, they spend their time eating three " +
		userWords[9] + " a day, and fathers play golf, and mothers sit around " +
		userWords[10] + ".\nLast summer, my little brother fell in a/an " +
		userWords[11] + " and got poison " +
		userWords[12] + " all over his " +
		userWords[13] + ".\nMy family is going to go to (the) " +
		userWords[14] + ", and I will practice " +
		userWords[15] + ".\nParents need vacations more than kids because parents are always very " +
		userWords[16] + " and because they have to work " +
		userWords[17] + " hours every day all year making enough " +
		userWords[18] + " to pay for the vacation.\n\n";
	return input;
}

int main()
{
	const int NUMBER_OF_WORDS = 19; //Set to number of words needed to complete Mad Lib

	string userWords[NUMBER_OF_WORDS]; // Create array using the number of words needed

	// Create array to store the couts to user so they know what input we're expecting.
	string display[NUMBER_OF_WORDS] =
	{
		"Enter an adjective: ", // index 0
		"Enter a adjective: ", // index 1
		"Enter a noun: ",  // index 2
		"Enter a noun: ", // index 3
		"Enter a plural noun: ", // index 4
		"Enter a game (noun): ",  // index 5
		"Enter a plural noun: ", // index 6
		"Enter a verb ending in 'ing': ",  // index 7
		"Enter a verb ending in 'ing': ",  // index 8
		"Enter a plural noun: ",  // index 9
		"Enter a verb ending in 'ing': ",  // index 10
		"Enter a noun: ",  // index 11
		"Enter a plant (noun): ",  // index 12
		"Enter a part of the body (noun): ",  // index 13
		"Enter a place (noun): ",  // index 14
		"Enter a verb ending in 'ing': ",  // index 15
		"Enter an adjective: ",  // index 16
		"Enter a number: ",  // index 17
		"Enter a plural noun: " // index 18
	};

	// Iterate through our array, displaying the indexed line of our array to the user and taking their input into the same index of another array
	for (int i = 0; i < NUMBER_OF_WORDS; i++)
	{
		cout << display[i];
		cin >> userWords[i];
	}

	// Pass the array name (which is itself a pointer) into our function, store returned value into a string pointer
	string *pOutput = BuildMadLib(userWords);
	// Dereference the string pointer and display output
	cout << "\n" + *pOutput;

	// Arbitrarily assigned yesNo to a character other than y or n to ensure while loop kicks off
	char yesNo = 'a';

	// If user does not enter a y, Y, n or N, keep prompting user to do so
	while (tolower(yesNo) != 'y' && tolower(yesNo) !=  'n' ) {
		cout << "Would you like to save to the MadLib to a file? (y/n): ";
		cin >> yesNo;
	}

	// If users enters y or Y, dereference pOutput and pass it into our function to save
	if (yesNo == 'y') SaveText(*pOutput);

	// Delete the pointer to release the memory taken up in the heap by the newed up string in BuildMadLib()
	delete pOutput;
	return 0;

}